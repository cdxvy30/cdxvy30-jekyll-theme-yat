---
layout: post
title: Class.Note-資料庫系統lec3:Relational Data Model
author: cdxvy30
categories: class
tags: [database systems]
---
# What is Relational Data Model?
資料模型用來隱藏資料的複雜度，隨整個application資料內容向下傳遞。

## Formal Definition of Relational Database
3個layers，5個定義。
1. Domain
2. Relation
  - Relation schema
  - Relation state
3. (Relational) database
  - Database schema
  - Database state

# Domain
想成一個attribute(但本質上有些許不同，後面會談)，有其特定的資料型別(type)和格式(format)，並且會有attibute name。例如密碼這個attibute，type是string，格式則是有長度限制，name為password。密碼就是application中所有密碼的集合，也就是domain。

# Relation Schema

# Relation State

# Relation Schema and States and Mathematical Definition

# Constraints

## Model Contraints

## What is a GOOD DATABASE MODEL?

## Schema-Based

## Key Constrains

## Constraints on NULL

## Entity Integrity Constraints

## Referential Integrity Constraints

# Relational Database

## Semantic Intergrity Constraints

## Functional Relationship

## State vs Transition Constraints

# Data Manipulation in RDM

## Constraint Violations

## Insert

## Delete

## Update

## Transaction Concept
