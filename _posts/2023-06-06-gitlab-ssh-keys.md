---
layout: post
title: 設定GitLab SSH keys並與GitLab溝通
author: cdxvy30
categories: techniques
tags: [Git, GitLab, SSH]
---

## 為何要設定SSH Keys?
其實先前在外出較常帶的Macbook Air已經有設定一組SSH Keys，在資訊所實習配發的電腦也有設定；但當在新買的桌電中要之後主要撰寫文章與開發環境的虛擬機中，......我還是忘記怎麼設定SSH Keys了，因此作為開發紀錄的第一篇文章，就簡單介紹一下SSH的概念、Public and Private Key(公私鑰)與如何設定和GitLab溝通的SSH Keys。

## 什麼是SSH
全名為Secure Shell或Secure Socket Shell，是一種經過加密的網路傳輸協定，

## 必要條件
* OpenSSH Client端，在GNU/Linux, macOS, Windows作業系統上應為pre-installed。
* SSH版本6.5或更高；較舊的版本使用的是MD5簽章，安全性較低。
* 若要查看在你的系統上運行的SSH版本，在終端機執行`ssh -V`。

## 支援的SSH key種類
GitLab提供下列6種SSH key支援:
* ED25519
* ED25519_SK
* ECDSA_SK
* RSA
* DSA
* ECDSA
就不介紹各種加密演算法了...因為我也不會，之後如果有幸涉足密碼學領域再好好研究...

## 產生一組SSH key pair
1. 開啟終端機
2. 執行`ssh-keygen -t`加上想要設定的key種類，

## 參考來源